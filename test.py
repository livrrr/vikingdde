#!/usr/bin/python
# -*- coding: windows-1251 -*-
import vikingdde

# Topic object should have "topic" attribute and "onPoke" method with 2 arguments
class Test:
	def __init__(self, topic):
		self.topic = topic

	def onPoke(self, row_col, data):
		print (row_col, data)

vikingdde.addTopic(Test('securities'))
vikingdde.start('viking')
while (True):
	vikingdde.process()
