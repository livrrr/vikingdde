#!/usr/bin/python
from distutils.core import setup, Extension
import sys, os, time

def get_path(name):
	for p in reversed(os.environ['PATH'].split(';')):
		for r,d,f in os.walk(p):
			for files in f:
				if files == name:
					return os.path.join(r, files)
	return ''

if len(sys.argv) == 1:
	sys.argv.append('build')
	sys.argv.append('--compiler=mingw32')
	#sys.argv.append('--compiler=msvc')
	#sys.argv.append('--compiler=unix')
	sys.argv.append('install')

	pthread = get_path('libwinpthread-1.dll')

	setup (name = 'vikingdde',
			version = '1.0',
			maintainer = 'Andrey V. Galushkin, Roman I. Liverovskiy',
			maintainer_email = 'galushkin@gmail.com, r.liverovskiy@fkviking.ru',
			description = 'Viking DDE server',
			data_files = [('Lib\\site-packages', [pthread]), ],
			ext_modules = [Extension('vikingdde', sources = ['vikingdde.cpp'],
#				include_dirs = ['C:/Python34/include'],
#				library_dirs = ['C:/Python34/libs'],
				extra_compile_args = ['-std=gnu++0x', ],#'-O1', '-fno-inline'
				extra_link_args = ['-static-libgcc', '-static-libstdc++']#, '-Wl,-Bstatic', '-lstdc++', '-lpthread', '-Wl,-Bdynamic']
				)]
			)

elif len(sys.argv) == 2 and sys.argv[1] == 'test':
	pass
