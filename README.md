# README #

This is a Python extension, written in C++. It is a DDE server with only POKE messages support, it works without PyWin32. Extension was written for use with QUIK trading terminal.

### Features ###

* Both Python 2 and 3 support
* DDE server, which works without PyWin32
* Works with PyQt4 and PyQt5 (unlike PyWin32 DDE server, which crashes sometimes)

### Install ###

Run in terminal
```
python setup.py
```

### Usage ###

Usage example is in file ```test.py```.

To view documentation run in python console

```
#!python
import vikingdde
help(vikingdde)
```