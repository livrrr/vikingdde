#include <algorithm>
#include <Python.h>
#include <windows.h>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <map>
#include <ddeml.h>

static DWORD serverId = 0;
static HSZ serverNameId = NULL;
static UINT isXlTable = 0;
static LPCTSTR serverName = "viking";
static LPCTSTR XlTable = "XlTable";

static std::map<std::string, PyObject*> topics;

static PyObject* vikingdde_start(PyObject* self, PyObject* args);
static PyObject* vikingdde_process(PyObject* self, PyObject* args);
static PyObject* vikingdde_stop(PyObject* self, PyObject* args);
static PyObject* vikingdde_add_topic(PyObject* self, PyObject* args);
static PyObject* vikingdde_del_topic(PyObject* self, PyObject* args);
static PyObject* vikingdde_clear_topics(PyObject* self, PyObject* args);

static PyMethodDef vikingdde_methods[] =
{
	{"start", vikingdde_start, METH_VARARGS, "Start DDE server in current thread"},
	{"process", vikingdde_process, METH_NOARGS, "Process DDE server event loop"},
	{"stop", vikingdde_stop, METH_NOARGS, "Stop DDE server"},
	{"addTopic", vikingdde_add_topic, METH_VARARGS, "Add topic"},
	{"delTopic", vikingdde_del_topic, METH_VARARGS, "Delete topic"},
	{"clearTopics", vikingdde_clear_topics, METH_NOARGS, "Delete all topics"},
	{NULL, NULL}
};

#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef vikingdde_module =
{
	PyModuleDef_HEAD_INIT,
	"vikingdde",    /* name of module */
	"Viking DDE server",  /* doc string, may be NULL */
	-1,
	vikingdde_methods,
};

PyMODINIT_FUNC PyInit_vikingdde(void)
{
	return PyModule_Create(&vikingdde_module);
}
#else
PyMODINIT_FUNC initvikingdde(void)
{
	PyImport_AddModule("vikingdde");
	Py_InitModule("vikingdde", vikingdde_methods);
}
#endif

void WriteLog_DDE(char* s, int i)
{
	printf(s);
	printf("\n");
}

HDDEDATA CALLBACK DdeCallback(UINT uType, UINT uFmt, HCONV hconv, HSZ hsz1, HSZ hsz2, HDDEDATA hData, ULONG_PTR dwData1, ULONG_PTR dwData2)
{
	switch (uType)
	{
		case XTYP_ADVDATA:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_ADVDATA", 0);
			return (HDDEDATA) DDE_FNOTPROCESSED;
		}
		case XTYP_ADVREQ:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_ADVREQ", 0);
			break;
		}
		case XTYP_ADVSTART:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_ADVSTART", 0);
			return (HDDEDATA) FALSE;
		}
		case XTYP_ADVSTOP: //This transaction is filtered if the server application specified the CBF_FAIL_ADVISES flag in the DdeInitialize function.
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_ADVSTOP", 0);
			break;
		}
		case XTYP_CONNECT:
		{
			if (DdeCmpStringHandles(serverNameId, hsz2) != 0)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> XTYP_CONNECT -> DdeCmpStringHandles(..)", -1);
				return (HDDEDATA) FALSE;
			}
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_CONNECT", 0);
			
			DWORD dwDataCnt = DdeQueryString(serverId, hsz1, NULL, 0, CP_WINANSI);
			if (dwDataCnt == 0)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> DdeQueryString(..)", dwDataCnt);
				return (HDDEDATA) FALSE;
			}

			// printf("dwDataCnt = %d", dwDataCnt);
			LPSTR lpTopic = (LPTSTR) malloc(dwDataCnt + 1);
			if (lpTopic == NULL)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> lpTopic = malloc(..)", 0);
				free(lpTopic);
				return (HDDEDATA) FALSE;
			}

			DdeQueryString(serverId, hsz1, lpTopic, dwDataCnt + 1, CP_WINANSI);
			if (dwDataCnt == 0)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> DdeQueryString(..)", dwDataCnt);
				free(lpTopic);
				return (HDDEDATA) FALSE;
			}

			std::string key = lpTopic;
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			//printf ("topic=%s\n", key.c_str());
			free(lpTopic);
			
			std::map<std::string, PyObject*>::iterator iter = topics.find(key);
			// TODO inform subscribers on connect
			if (topics.end() == iter)
			{
				return (HDDEDATA) FALSE;
			}
			
			return (HDDEDATA) TRUE;
		}
		case XTYP_CONNECT_CONFIRM: //This transaction is filtered if the server application specified the CBF_SKIP_CONNECT_CONFIRMS flag in the DdeInitialize function.
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_CONNECT_CONFIRM", 0);
			break;
		}
		case XTYP_DISCONNECT: //This transaction is filtered if the application specified the CBF_SKIP_DISCONNECTS flag in the DdeInitialize function.
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_DISCONNECT", 0);
			// TODO inform subscribers on disconnect
			break;
		}
		case XTYP_ERROR:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_ERROR", 0);
			break;
		}
		case XTYP_EXECUTE:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_EXECUTE", 0);
			return (HDDEDATA) DDE_FNOTPROCESSED;
		}
		case XTYP_MONITOR:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_MONITOR", 0);
			return (HDDEDATA) 0;
		}
		case XTYP_POKE:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_POKE", 0);
			PyGILState_STATE gstate = PyGILState_Ensure();

			//Проверяем, что тип данных подходит ("XlTable")
			if (isXlTable != uFmt)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> isXlTable != uFmt -> uFmt", uFmt);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}

			DWORD dwDataCnt = DdeQueryString(serverId, hsz1, NULL, 0, CP_WINANSI);
			if (dwDataCnt == 0)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> DdeQueryString(..)", dwDataCnt);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}

			// printf("dwDataCnt = %d", dwDataCnt);
			LPSTR lpTopic = (LPTSTR) malloc(dwDataCnt + 1);
			if (lpTopic == NULL)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> lpTopic = malloc(..)", 0);
				free(lpTopic);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}

			DdeQueryString(serverId, hsz1, lpTopic, dwDataCnt + 1, CP_WINANSI);
			if (dwDataCnt == 0)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> DdeQueryString(..)", dwDataCnt);
				free(lpTopic);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}

			std::string key = lpTopic;
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			free(lpTopic);

			dwDataCnt = DdeQueryString(serverId, hsz2, NULL, 0, CP_WINANSI);
			if (dwDataCnt == 0)
			{
				// WriteLog_DDE((char *) "[ERR] DdeCallback(..) -> DdeQueryString(..)", dwDataCnt);
				PyGILState_Release(gstate);
				return(HDDEDATA) DDE_FNOTPROCESSED;
			}

			LPSTR lpR1C1R2C4 = (LPTSTR) malloc(dwDataCnt + 1);
			if (lpR1C1R2C4 == NULL)
			{
				// WriteLog_DDE((char *) "[ERR] DdeCallback(..) -> lpR1C1R2C4 = malloc(..)", 0);
				free(lpR1C1R2C4);
				PyGILState_Release(gstate);
				return(HDDEDATA) DDE_FNOTPROCESSED;
			}

			DdeQueryString(serverId, hsz2, lpR1C1R2C4, dwDataCnt + 1, CP_WINANSI);
			if (dwDataCnt == 0)
			{
				// WriteLog_DDE((char *) "[ERR] DdeCallback(..) -> DdeQueryString(..)", dwDataCnt);
				free(lpR1C1R2C4);
				PyGILState_Release(gstate);
				return(HDDEDATA) DDE_FNOTPROCESSED;
			}
			
			std::string row_col = lpR1C1R2C4;
			free(lpR1C1R2C4);
			
			dwDataCnt = DdeGetData(hData, NULL, sizeof(DWORD), 0);
			if (dwDataCnt == DMLERR_DLL_NOT_INITIALIZED || dwDataCnt == DMLERR_INVALIDPARAMETER || dwDataCnt == 0)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> DdeGetData(..)", dwDataCnt);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}

			LPBYTE lpbBufData = (LPBYTE) malloc(dwDataCnt);
			if (lpbBufData == NULL)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> lpbBufData = malloc(..)", 0);
				free(lpbBufData);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}
			memset(lpbBufData, 0, dwDataCnt);

			DWORD dwRet = DdeGetData(hData, lpbBufData, dwDataCnt, 0);
			if (dwRet == DMLERR_DLL_NOT_INITIALIZED || dwRet == DMLERR_INVALIDPARAMETER || dwRet == 0)
			{
				// WriteLog_DDE((char*) "[ERR] DdeCallback(..) -> DdeGetData(..)", dwRet);
				free(lpbBufData);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}

			std::map<std::string, PyObject*>::iterator iter = topics.find(key);
			if (topics.end() != iter)
			{
				PyObject* f = PyObject_GetAttrString(iter->second, "onPoke");
				if (NULL != f)
				{
					Py_DECREF(f);
#if PY_MAJOR_VERSION >= 3
					PyObject* res = PyObject_CallMethod(iter->second, (char*)"onPoke", (char*)"(sy#)", row_col.c_str(), lpbBufData, dwDataCnt);
#else
					PyObject* res = PyObject_CallMethod(iter->second, (char*)"onPoke", (char*)"(sz#)", row_col.c_str(), lpbBufData, dwDataCnt);
#endif
					Py_XDECREF(res);
				}
				else
				{
					free(lpbBufData);
					PyGILState_Release(gstate);
					return (HDDEDATA) DDE_FNOTPROCESSED;
				}
			}
			else
			{
				free(lpbBufData);
				PyGILState_Release(gstate);
				return (HDDEDATA) DDE_FNOTPROCESSED;
			}

			// printf("dwDataCnt = %d", dwDataCnt);

			free(lpbBufData);
			PyGILState_Release(gstate);
			return (HDDEDATA) DDE_FACK;
		}
		case XTYP_REGISTER: //This transaction is filtered if the application specified the CBF_SKIP_REGISTRATIONS flag in the DdeInitialize function.
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_REGISTER", 0);
			break;
		}
		case XTYP_REQUEST:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_REQUEST", 0);
			break;
		}
		case XTYP_UNREGISTER: //This transaction is filtered if the application specified the CBF_SKIP_REGISTRATIONS flag in the DdeInitialize function.
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_UNREGISTER", 0);
			break;
		}
		case XTYP_WILDCONNECT:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_WILDCONNECT", 0);
			break;
		}
		case XTYP_XACT_COMPLETE:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> XTYP_XACT_COMPLETE", 0);
			break;
		}
		default:
		{
			// WriteLog_DDE((char*) "[DBG] DdeCallback(..) -> default", 0);
			break;
		}
	}
	return (HDDEDATA) NULL;
}

static PyObject* vikingdde_start(PyObject* self, PyObject* args)
{
	if (serverId != 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "DDE server is already running");
		return NULL;
	}
	const char* name;
	if (!PyArg_ParseTuple(args, "z", &name))
	{
		PyErr_SetString(PyExc_RuntimeError, "Wrong function attributes");
		return NULL;
	}
	if (NULL != name)
	{
		serverName = name;
	}
	//uRet = DdeInitializeA(&serverId, (PFNCALLBACK) DdeCallback, APPCLASS_STANDARD | MF_CALLBACKS | MF_CONV | MF_ERRORS | MF_HSZ_INFO | MF_LINKS | MF_POSTMSGS | MF_SENDMSGS, 0);
	UINT uRet = DdeInitialize(&serverId, (PFNCALLBACK) DdeCallback, APPCLASS_STANDARD, 0);
	if (uRet != DMLERR_NO_ERROR)
	{
		char tmp[256];
		sprintf (tmp, "Cann't initializ DDE, err_code=%u", uRet);
		PyErr_SetString(PyExc_RuntimeError, tmp);
		return NULL;
	}
	//cout << "serverId = " << serverId << endl;

	//Naming server
	serverNameId = DdeCreateStringHandle(serverId, serverName, CP_WINANSI);
	if (serverNameId == 0)
	{
		DdeUninitialize(serverId);
		char tmp[256];
		sprintf (tmp, "Cann't create DDE server name, err_code=%u", DdeGetLastError(serverId));
		PyErr_SetString(PyExc_RuntimeError, tmp);
		return NULL;
	}

	//Naming server
	//cout << "serverNameId = " << serverNameId << endl;
	HDDEDATA hDdeData = DdeNameService(serverId, serverNameId, 0, DNS_REGISTER);
	if (hDdeData == 0)
	{
		DdeFreeStringHandle(serverId, serverNameId);
		DdeUninitialize(serverId);
		char tmp[256];
		sprintf (tmp, "Cann't set DDE server name, err_code=%u", DdeGetLastError(serverId));
		PyErr_SetString(PyExc_RuntimeError, tmp);
		return NULL;
	}

	//Для проверки, что тип данных "XlTable"
	isXlTable = RegisterClipboardFormat((LPSTR) XlTable);
	if (isXlTable == 0)
	{
		DdeNameService(serverId, 0, 0, DNS_UNREGISTER);
		DdeFreeStringHandle(serverId, serverNameId);
		DdeUninitialize(serverId);
		char tmp[256];
		sprintf (tmp, "Cann't register clipboard format, err_code=%u", DdeGetLastError(serverId));
		PyErr_SetString(PyExc_RuntimeError, tmp);
		return NULL;
	}

	return PyLong_FromLong(0L);
}

static PyObject* vikingdde_process(PyObject* self, PyObject* args)
{
	if (serverId == 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "DDE server is not running");
		return NULL;
	}
	MSG msg;
	//while (GetMessage(&msg, NULL, 0, 0) > 0)
	if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	//if (GetMessage(&msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	/*if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}*/
	return PyLong_FromLong(0L);
}

static PyObject* vikingdde_stop(PyObject* self, PyObject* args)
{
	if (serverId == 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "DDE server is not running");
		return NULL;
	}
	DdeFreeStringHandle(serverId, serverNameId);
	DdeNameService(serverId, 0, 0, DNS_UNREGISTER);
	DdeUninitialize(serverId);
	serverId = 0;
	return PyLong_FromLong(0L);
}

static PyObject* vikingdde_add_topic(PyObject* self, PyObject* args)
{
	PyObject* topic = NULL;
	if (!PyArg_ParseTuple(args, "O", &topic))
	{
		PyErr_SetString(PyExc_RuntimeError, "Wrong function attributes");
		return NULL;
	}
	
	PyObject* f = PyObject_GetAttrString(topic, "onPoke");
	if (NULL != f)
	{
		Py_DECREF(f);
	}
	else
	{
		PyErr_SetString(PyExc_RuntimeError, "Cann't find \"onPoke\" method in topic object");
		return NULL;
	}

	std::string key = "";
	PyObject* name = PyObject_GetAttrString(topic, "topic");
	if (NULL == name)
	{
		PyErr_SetString(PyExc_RuntimeError, "Topic object has no \"topic\" attribute");
		return NULL;
	}

#if PY_MAJOR_VERSION >= 3
	if (PyUnicode_Check(name))
	{
		PyObject* temp_bytes = PyUnicode_AsEncodedString(name, "ASCII", "strict");  // Owned reference
		if (NULL != temp_bytes)
		{
			key = PyBytes_AS_STRING(temp_bytes); // Borrowed pointer
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			if (topics.end() == topics.find(key))
			{
				topics[key] = topic;
				Py_XINCREF(topics[key]);
			}
			else
			{
				Py_XDECREF(topics[key]);
				topics[key] = topic;
				Py_XINCREF(topics[key]);
			}
			Py_DECREF(temp_bytes);
			Py_DECREF(name);
		}
		else
		{
			Py_DECREF(name);
			PyErr_SetString(PyExc_RuntimeError, "Topic name should be unicode");
			return NULL;
		}
	}
	else
	{
		Py_DECREF(name);
		PyErr_SetString(PyExc_RuntimeError, "Topic name should be unicode");
		return NULL;
	}
#else
	char* n = PyString_AsString(name);
	if (NULL != n)
	{
		key = n;
		std::transform(key.begin(), key.end(), key.begin(), ::tolower);
		if (topics.end() == topics.find(key))
		{
			topics[key] = topic;
			Py_XINCREF(topics[key]);
		}
		else
		{
			Py_XDECREF(topics[key]);
			topics[key] = topic;
			Py_XINCREF(topics[key]);
		}
		Py_DECREF(name);
	}
	else
	{
		Py_DECREF(name);
		PyErr_SetString(PyExc_RuntimeError, "Topic name should be str or unicode object");
		return NULL;
	}
#endif

	return PyLong_FromLong(0L);
}

static PyObject* vikingdde_del_topic(PyObject* self, PyObject* args)
{
	PyObject* topic = NULL;
	if (!PyArg_ParseTuple(args, "O", &topic))
	{
		PyErr_SetString(PyExc_RuntimeError, "Wrong function attributes \"topic\" attribute");
		return NULL;
	}

	std::string key = "";
	PyObject* name = PyObject_GetAttrString(topic, "topic");
	if (NULL == name)
	{
		PyErr_SetString(PyExc_RuntimeError, "Wrong function attributes");
		return NULL;
	}

#if PY_MAJOR_VERSION >= 3
	if (PyUnicode_Check(name))
	{
		PyObject* temp_bytes = PyUnicode_AsEncodedString(name, "ASCII", "strict");  // Owned reference
		if (NULL != temp_bytes)
		{
			key = PyBytes_AS_STRING(temp_bytes); // Borrowed pointer
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			//topics[key] = topic;
			std::map<std::string, PyObject*>::iterator iter = topics.find(key);
			if (topics.end() != iter)
			{
				PyObject* o = iter->second;
				topics.erase(key);
				Py_XDECREF(o);
			}
			Py_DECREF(temp_bytes);
			Py_DECREF(name);
		}
		else
		{
			Py_DECREF(name);
			PyErr_SetString(PyExc_RuntimeError, "Topic name should be unicode");
			return NULL;
		}
	}
	else
	{
		Py_DECREF(name);
		PyErr_SetString(PyExc_RuntimeError, "Topic name should be unicode");
		return NULL;
	}
#else
	char* n = PyString_AsString(name);
	if (NULL != n)
	{
		key = n;
		std::transform(key.begin(), key.end(), key.begin(), ::tolower);
		//topics[key] = topic;
		std::map<std::string, PyObject*>::iterator iter = topics.find(key);
		if (topics.end() != iter)
		{
			PyObject* o = iter->second;
			topics.erase(key);
			Py_XDECREF(o);
		}
		Py_DECREF(name);
	}
	else
	{
		Py_DECREF(name);
		PyErr_SetString(PyExc_RuntimeError, "Topic name should be str or unicode object");
		return NULL;
	}
#endif

	return PyLong_FromLong(0L);
}

static PyObject* vikingdde_clear_topics(PyObject* self, PyObject* args)
{
	for (std::map<std::string, PyObject*>::iterator iter = topics.begin(); iter != topics.end(); ++iter)
	{
		Py_XDECREF(iter->second);
	}
	topics.clear();

	return PyLong_FromLong(0L);
}

